#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta

__all__ = ['Sale']
__metaclass__ = PoolMeta

class Sale:
    __name__ = 'sale.sale'

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        for field_name in ('carrier', 'party', 'currency', 'sale_date', 
            'shipment_cost_method', 'lines', 'shipment_address'):
            if field_name not in cls.lines.on_change:
                cls.lines.on_change.append(field_name)
        for field_name in cls.lines.on_change:
            if field_name not in cls.carrier.on_change:
                cls.carrier.on_change.append(field_name)

    def _get_carrier_context(self):
        context = super(Sale, self)._get_carrier_context()

        if self.carrier.carrier_cost_method != 'grid':
            return context
        context = context.copy()
        # context['warehouse_zip'] = self.warehouse.address.zip
        context['shipment_zip'] = self.shipment_address.zip

        return context
