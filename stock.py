#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta

__all__ = ['ShipmentOut', 'ShipmentIn']
__metaclass__ = PoolMeta

class ShipmentOut:
    __name__ = 'stock.shipment.out'

    @classmethod
    def __setup__(cls):
        super(ShipmentOut, cls).__setup__()
        for field_name in ('carrier', 'delivery_address'):
            if field_name not in cls.lines.on_change:
                cls.lines.on_change.append(field_name)
        for field_name in cls.lines.on_change:
            if field_name not in cls.carrier.on_change:
                cls.carrier.on_change.append(field_name)

    def _get_carrier_context(self):
        context = super(ShipmentOut, self)._get_carrier_context()
        if not self.carrier:
            return context
        if self.carrier.carrier_cost_method != 'grid':
            return context
        context = context.copy()
        # context['warehouse_zip'] = self.warehouse.address.zip
        context['shipment_zip'] = self.delivery_address.zip

        return context

class ShipmentIn:
    __name__ = 'stock.shipment.in'

    @classmethod
    def __setup__(cls):
        super(ShipmentIn, cls).__setup__()
        for field_name in ('carrier', 'incoming_moves', 'supplier_location', 'supplier'):
            if field_name not in cls.lines.on_change:
                cls.lines.on_change.append(field_name)
        for field_name in cls.lines.on_change:
            if field_name not in cls.carrier.on_change:
                cls.carrier.on_change.append(field_name)

    def _get_carrier_context(self):
        context = super(ShipmentOut, self)._get_carrier_context()
        if not self.carrier:
            return context
        if self.carrier.carrier_cost_method != 'grid':
            return context
        context = context.copy()
        # context['warehouse_zip'] = self.warehouse.address.zip
        context['shipment_zip'] = self.supplier_location.address.zip

        return context

