#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval, Bool, Id
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = ['Carrier', 'PostalCodePriceList']
__metaclass__ = PoolMeta


class Carrier:
    __name__ = 'carrier'
    postal_code_currency = fields.Many2One('currency.currency', 'Moneda',
        states={
            'invisible': Eval('carrier_cost_method') != 'grid',
            'required': Eval('carrier_cost_method') == 'grid',
            # 'readonly': Bool(Eval('postal_code_price_list', [])),
            },
        depends=['carrier_cost_method', 'postal_code_price_list'])
    postal_code_currency_digits = fields.Function(fields.Integer(
            'Postal Code Currency Digits', on_change_with=['postal_code_currency']),
        'on_change_with_postal_code_currency_digits')
    postal_code_price_list = fields.One2Many('carrier.postal_code_price_list', 'carrier',
        'Matriz de precios segun codigos postales',
        states={
            'invisible': Eval('carrier_cost_method') != 'grid',
            },
        depends=['carrier_cost_method'])

    @classmethod
    def __setup__(cls):
        super(Carrier, cls).__setup__()
        selection = ('grid', 'Matriz')
        if selection not in cls.carrier_cost_method.selection:
            cls.carrier_cost_method.selection.append(selection)

    @staticmethod
    def default_postal_code_currency_digits():
        Company = Pool().get('company.company')
        company = Transaction().context.get('company')
        if company:
            return Company(company).currency.digits
        return 2

    @classmethod
    def get_available_carriers(cls, postal_code):
        'Returns available carriers for received postal code. Postal code must have country prefix.'
        carriers = cls.search([('party.active', '=', True)])
        res = []
        for carrier in carriers:
            if carrier.check_available(int(postal_code[2:])):
                res.append(carrier.id)
        return res

    @classmethod
    def check_available_carriers(cls, postal_code):
        'Checks available carriers for received postal code. Postal code must have country prefix.'
        carriers = cls.search([('party.active', '=', True)])
        for carrier in carriers:
            if carrier.check_available(int(postal_code[2:])):
                return True
        return False

    def on_change_with_postal_code_currency_digits(self, name=None):
        if self.postal_code_currency:
            return self.postal_code_currency.digits
        return 2

    def check_available(self, postal_code):
        'Checks if the carrier has the received postal code available for delivery. Postal code must have country prefix.'
        for line in self.postal_code_price_list:
            if int(line.start_postal_code[2:]) <= postal_code <= int(line.end_postal_code[2:]):
                return True
        return False

    def compute_grid_sale_price(self, postal_code):
        'Compute price based on postal codes'
        for line in self.postal_code_price_list:
            if int(line.start_postal_code[2:]) <= int(postal_code[2:]) <= int(line.end_postal_code[2:]):
                return line.sale_price
        return Decimal(0)

    def compute_grid_cost_price(self, postal_code):
        'Compute price based on postal codes'
        for line in self.postal_code_price_list:
            if int(line.start_postal_code[2:]) <= int(postal_code[2:]) <= int(line.end_postal_code[2:]):
                return line.cost_price
        return Decimal(0)

    def get_sale_price(self):
        'Compute carrier sale price with currency'
        price, currency_id = super(Carrier, self).get_sale_price()
        shipment_postal_code = Transaction().context.get('shipment_zip')
        if self.carrier_cost_method == 'grid':
            postal_code_price = Decimal(0)
            postal_code_price += self.compute_grid_sale_price(shipment_postal_code)
            return postal_code_price, self.postal_code_currency.id
        return price, currency_id

    def get_purchase_price(self):
        'Compute carrier purchase price with currency'
        price, currency_id = super(Carrier, self).get_sale_price()
        shipment_postal_code = Transaction().context.get('shipment_zip')
        if self.carrier_cost_method == 'grid':
            postal_code_price = Decimal(0)
            postal_code_price += self.compute_grid_cost_price(shipment_postal_code)
            return postal_code_price, self.postal_code_currency.id
        return price, currency_id

class PostalCodePriceList(ModelSQL, ModelView):
    'Postal code price matrix'
    __name__ = 'carrier.postal_code_price_list'
    carrier = fields.Many2One('carrier', 'Carrier', required=True, select=True)
    start_postal_code = fields.Char('Codigo postal inicial', required=True)
    end_postal_code = fields.Char('Codigo postal final', required=True)
    sale_price = fields.Numeric('Precio de venta', digits=(16, Eval('_parent_carrier.postal_code_currency_digits', 2)), required=True)
    cost_price = fields.Numeric('Precio de coste', digits=(16, Eval('_parent_carrier.postal_code_currency_digits', 2)), required=True)
