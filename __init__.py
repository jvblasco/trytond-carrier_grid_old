#This file is part country_zip module for Tryton.
#The COPYRIGHT file at the top level of this repository contains 
#the full copyright notices and license terms.
from trytond.pool import Pool
from .carrier import *
from .sale import *
from .stock import *

def register():
    Pool.register(
        Carrier,
        PostalCodePriceList,
        Sale,
        ShipmentOut,
        ShipmentIn,
        module='carrier_grid', type_='model')
